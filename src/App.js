import React, { Component } from 'react';
import {
  Button,
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Label
} from 'reactstrap';
import {
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback
} from 'availity-reactstrap-validation';
import {
  getCamps,
  createCamp,
  updateCamp
} from './helpers/api';
import LocationPicker from 'react-location-picker';
import ReactTable from "react-table";
import "react-table/react-table.css";

var defaultPosition = {
  lat: 10.056154806166639,
  lng: 76.33555297851558
};

class App extends Component {
  constructor(props){
      super(props);
      this.state = ({
        rescueCamps : [],
        showCreateNewCampModel: false,
        showLocationMapModel: false,
        campAddress: '',
        position: {
          lat: 0,
          lng: 0
        },
        viewPosition: null,
        modelTitle: 'Create New',
        editData: {}
      });
  }

  componentDidMount = () => {
    this.loadDataGrid();
  }

  loadDataGrid = () => {
    getCamps().then(response => {
      this.setState({rescueCamps: response.data});
    }).catch(error => {
      console.log(error);
    })
  }

  handleLocationChange = ({ position, address }) => {
    this.setState({campAddress: address, position: position});
  }

  toggleForm = (type='new') => {
    if(type === 'new'){
      this.setState({
        showCreateNewCampModel: !this.state.showCreateNewCampModel,
        editData: {},
        modelTitle: 'Create '
      });
    } else {
      this.setState({
        showCreateNewCampModel: !this.state.showCreateNewCampModel
      });
    }
  }

  toggleLocationMap = (event,row) => {
    let location = row.original.location;
    let position = {};
    position.lat = parseFloat(location.lat);
    position.lng = parseFloat(location.lng);
    console.log(defaultPosition);
    console.log(position)
    this.setState({viewPosition:position}, () => {
      alert("test");
      this.setState({showLocationMapModel: !this.state.showLocationMapModel});
    });
    //this.setState({showLocationMapModel: !this.state.showLocationMapModel, campAddress:location.address, viewPosition:position});
  }

  triggerEdit = (event, row) => {
    let position = {};
    position.lat = parseFloat(row.original.location.lat);
    position.lng = parseFloat(row.original.location.lng);
    this.setState({modelTitle: 'Update ', editData: row.original, viewPosition:position});
    this.toggleForm('edit');
  }

  formSubmit = (event, values) => {
      this.toggleForm();
      let payload = {};
      let location = {};
      payload.id = values.id;
      payload.name = values.name;
      payload.address = values.address;
      payload.capacity = values.capacity;
      payload.contact_numbers = values.numbers;
      payload.we_want = values.want;
      payload.we_have = values.have;

      location.lat = this.state.position.lat;
      location.lng = this.state.position.lng;
      location.address = this.state.campAddress;

      payload.location = location;
      if(values.id.length > 3) {
        updateCamp(payload.id, payload).then(response =>{
          this.loadDataGrid()
        }).catch(error => {
          console.log(error)
          alert("something went wrong")
        })
      } else {
        createCamp(payload).then(response => {
          this.loadDataGrid();
        }).catch(error => {
          console.log(error);
          alert("something went wrong");
        })
      }
  }

  render() {

    const columns = [{
      Header: 'Name',
      accessor: 'name',
      style: css.tableRow
    }, {
      Header: 'Address',
      accessor: 'address',
      style: css.tableRow
    }, {
      Header: 'Contact Nos.',
      accessor: 'contact_numbers',
      style: css.tableRow
    }, {
      Header: 'We want',
      accessor: 'we_want',
      style: css.tableRow
    }, {
      Header: 'We have',
      accessor: 'we_have',
      style: css.tableRow
    }, {
      Header: 'Updated At',
      accessor: 'updatedAt',
      style: css.tableRow
    }, {
      Header: 'Created At',
      accessor: 'createdAt',
      style: css.tableRow
    }, {
      Header: 'Options',
      accessor: 'id',
      Cell: row => {
                return (
                    <div>
                    <Button color="default" outline size="sm" onClick={(event)=>this.triggerEdit(event, row)}>Update</Button>
                    </div>
                    );
            },
      style: css.tableRow
    }];

    if (this.state.viewPosition) {
      defaultPosition = this.state.viewPosition;
    }

    return (
      <Container style={css.container}>
        <Row>
          <Col><Button color="danger" className="float-right" onClick={() => this.toggleForm('new')} style={css.createCamp}>Add New Rescue Camp</Button></Col>
        </Row>
        <Row>
          <Col>
          <Card>
            <CardHeader>
                Rescue Camps
            </CardHeader>
            <CardBody>
                <ReactTable
                    data={this.state.rescueCamps}
                    columns={columns}
                    defaultPageSize={10}
                    className="-striped -highlight"
                />
            </CardBody>
          </Card>
          </Col>
        </Row>
        <Modal isOpen={this.state.showCreateNewCampModel} toggle={this.toggleForm} className="modal-lg">
        <AvForm onValidSubmit={this.formSubmit} className="form-horizontal">
          <ModalHeader toggle={this.toggleForm}>{this.state.modelTitle} {(typeof this.state.editData.name === 'undefined'?'Camp':this.state.editData.name)}</ModalHeader>
          <ModalBody>
            <Row>
            <Col md="6">
              <AvGroup >
                  <Label htmlFor="text-input">Name *</Label>
                  <AvInput required name="name" id="name"  value={(typeof this.state.editData.name === 'undefined'?'':this.state.editData.name)}/>
                  <AvFeedback>Name is required</AvFeedback>
              </AvGroup>
              </Col>
              <Col md="6">
              <AvGroup >
                  <Label htmlFor="text-input">Contact Numbers *</Label>
                  <AvInput required name="numbers" id="numbers" value={(typeof this.state.editData.contact_numbers === 'undefined'?'':this.state.editData.contact_numbers)} />
                  <AvFeedback>Contact Numbers are required</AvFeedback>
              </AvGroup>
              </Col>
              <Col md="6">
              <AvGroup>
                  <Label htmlFor="text-input">We want</Label>
                  <AvInput type="textarea" name="want" id="want" value={(typeof this.state.editData.we_want === 'undefined'?'':this.state.editData.we_want)} />
              </AvGroup>
              </Col>
              <Col md="6">
              <AvGroup>
                  <Label htmlFor="text-input">We have</Label>
                  <AvInput type="textarea" name="have" id="have" value={(typeof this.state.editData.we_have === 'undefined'?'':this.state.editData.we_have)} />
              </AvGroup>
              </Col>
              <Col md="12">
              <Label htmlFor="text-input">Address (automatically generated)</Label>
                <Label htmlFor="text-input">{(typeof this.state.editData.address === 'undefined'?this.state.campAddress:this.state.editData.address)}</Label>
              </Col>
              <Col md="12">
              <Label htmlFor="text-input">Location</Label>
                <LocationPicker
                  containerElement={ <div style={ {height: '100%'} } /> }
                  mapElement={ <div style={ {height: '300px'} } /> }
                  defaultPosition={defaultPosition}
                  onChange={this.handleLocationChange}
                />
              </Col>
            </Row>
            </ModalBody>
            <ModalFooter>
              <AvInput type="hidden" name="address" value={this.state.campAddress} id="address" />
              <AvInput type="hidden" name="id" value={(typeof this.state.editData.id === 'undefined'?'':this.state.editData.id)} id="address" />
              <Button color="primary">Save</Button>{' '}
              <Button color="secondary" onClick={this.toggleForm}>Cancel</Button>
          </ModalFooter>
          </AvForm>
          </Modal>
      </Container>
    );
  }
}

export default App;

const css = {
  container: {
    marginTop: '20px'
  },
  tableRow: {
    textAlign: 'center'
  },
  createCamp: {
    marginBottom: '20px'
  }
}
