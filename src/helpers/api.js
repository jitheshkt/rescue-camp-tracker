import axios from 'axios';
var BASE_URL = 'http://51.15.209.239:3030';
//var BASE_URL = 'http://localhost:3030';

export function getCamps() {
    return axios.get(BASE_URL + '/camps/all').then((response) => {
        return response.data;
    });
}

export function createCamp(payload) {
    return axios.post(BASE_URL + '/camps/new', payload).then((response) => {
        return response.data;
    });
}

export function updateCamp(id,payload) {
    return axios.post(BASE_URL + '/camps/update/'+id, payload).then((response) => {
        return response.data;
    });
}